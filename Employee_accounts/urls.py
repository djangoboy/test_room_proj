from django.conf.urls import url
from . import views



urlpatterns = (
    # urls for Employee
    url(r'^employee/$', views.EmployeeListView.as_view(), name='Employee_accounts_employee_list'),
   )

#which means we can append more urls that can be specific to any apps
urlpatterns += (
    # urls for Employee
    url(r'^list/$', views.EmployeeListView.as_view(), name='Employee_accounts_employee_list'),
    #url(r'^employee/list/$', views.employeelistview, name='employeelistview'),
    #class createview
    url(r'^employee/cb/create/$', views.EmployeeCreateView.as_view(), name='Employee_accounts_employee_create'),
    #func createview
    url(r'^employee/fb/create/$', views.employeecreateview, name='employeecreateview'),

    url(r'^cb/detail/(?P<pk>[0-9]+)/$', views.EmployeeDetailView.as_view(), name='Employee_accounts_employee_detail'),
    url(r'^fb/detail/(?P<pk>[0-9]+)/$', views.employeeDetailView, name='employeeDetailView'),

    url(r'^update/(?P<slug>\S+)/$', views.EmployeeUpdateView.as_view(), name='Employee_accounts_employee_update'),
)
