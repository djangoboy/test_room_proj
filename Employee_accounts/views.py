from django.views.generic import DetailView, ListView, UpdateView, CreateView
from .models import Employee
from .forms import EmployeeForm
from django.shortcuts import render, HttpResponse, redirect, reverse


class EmployeeListView(ListView):
    model = Employee
    template_name = 'Employee_accounts/employee_list_func.html'
    context_object_name = 'all_emp'

#list function view
def employeelistview(request):
    all_emp = Employee.objects.all()
    return render(request, template_name='Employee_accounts/employee_list.html', context={'object_list':all_emp})

class EmployeeCreateView(CreateView):
    model = Employee
    form_class = EmployeeForm

#create function view
def employeecreateview(request):
    if request.method =='GET':
        form = EmployeeForm()
        return render(request, template_name='Employee_accounts/employee_form.html', context={'form': form})

    if request.method =='POST':
        form = EmployeeForm(request.POST)
        form.save()
        return redirect(reverse('accounts:Employee_accounts_employee_list'))

class EmployeeDetailView(DetailView):
    model = Employee

#Employee
def employeeDetailView(request, pk):
    emp = Employee.objects.get(pk=pk)
    template_name = 'Employee_accounts/employee_detail.html'
    return render(request, template_name=template_name, context={'emp':emp})


class EmployeeUpdateView(UpdateView):
    model = Employee
    form_class = EmployeeForm


#ListView, TemplateView, CreateView, UpdateView, DeleteView