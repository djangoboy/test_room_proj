from django.views.generic import DetailView, ListView, UpdateView, CreateView, TemplateView
from .models import Author, Post
from .forms import AuthorForm
from django.shortcuts import render, redirect, reverse
from .models import Form_Data
from .forms import UnboundForm, boundForm, PostboundForm
# from overriding.views import OwnedObjectMixin
from django.contrib import messages
from django.shortcuts import Http404, HttpResponse


class OwnedObjectMixin(object):
    def get_queryset(self, *args, **kwargs):
        if not obj.user == self.request.user:
            raise Http404

        return self.get_queryset().filter(active=True)

        return obj


class PostListView(ListView):
    model = Post


class Protected_post_list(OwnedObjectMixin, ListView):
    model = Author


































def home_basic_form(request): #i am unable to display the data
    if request.method == 'GET':
        messages.success(request, 'fill up this form!')
        return render(request, 'forms_test/home.html')
    else:
        #WHAT IF WE WANT TO SAVE DATA IN DATABASE
        form_data = Form_Data()
        form_data.name = request.POST['form_data']
        form_data.save()
        messages.success(request, 'YOur data is successfully saved!')
        objs = Form_Data.objects.all
        context = {'name': request.POST['form_data'], 'objs': objs}
        return render(request, 'forms_test/home.html', context=context)

class HomeView(TemplateView):
    template_name = 'forms_test/home.html'

    def get(self, request, *args, **kwargs):#def get(self, request, *args, **kwargs):
        template_name = 'forms_test/PostunboundForm.html'
        form = PostboundForm()
        posts = Post.objects.all()
        print(posts)
        return render(request, template_name=template_name, context={'form': form, 'posts': posts})

    def post(self, request, *args, **kwargs):#def get(self, request, *args, **kwargs):
        template_name = 'forms_test/PostunboundForm.html'
        form = PostboundForm(request.POST)
        if form.is_valid():
            print(request.user)
            post_obj = form.save(commit=False) #it returns the post object
            post_obj.author = request.user
            post_obj.save()
            form = PostboundForm()
        return render(request, template_name=template_name, context={'form':form})

            # return redirect(reverse('forms_test:forms_test_author_list'))
            # return redirect('/forms_test/author/cb/list/')



class AuthorListView(ListView):
    model = Author

    # < --> by default
    #context_= Author.objects.get_queryset()
    #template_name = 'forms_test/author_list.html'
    #self.object_list = self.get_queryset()


#function based views
def authorListView(request):
    template_name = 'forms_test/author_list.html'
    context = {'object_list': Author.objects.get_queryset()}
    return render(request, template_name=template_name, context=context)





class AuthorDetailView(DetailView):
    model = Author

def authorDetailView(request, pk):
    author = Author.objects.get(pk=pk)
    template_name = 'forms_test/author_detail.html'
    context = {'object': author}
    return render(request, template_name=template_name, context=context)


class AuthorCreateView(CreateView):
    model = Author
    form_class = AuthorForm


class AuthorUpdateView(UpdateView):
    model = Author
    form_class = AuthorForm


class ProtectedDetailView(DetailView):
    model = Author


#FORM HANDLING VIEWS
#can we handle this in a class

def unbound_form(request): #i am unable to display the data
    if request.method == 'GET':
        form = UnboundForm()
        context = {'form': form}
        return render(request, 'forms_test/unbound_form.html', context=context)
    else:
        form = UnboundForm()
        print(request.POST)
        author = Author()
        author.name = request.POST
        author.save()
        print(Author.objects.all())
        context = {'form': form, 'name': request.POST}
        return render(request, 'forms_test/unbound_form.html', context=context)

def bound_form(request): #i am unable to display the data
    if request.method == 'GET':
        form = boundForm()
        context = {'form': form}
        return render(request, 'forms_test/bound_form.html', context=context)
    else:
        form = boundForm(request.POST)
        # author = Form_Data()
        if form.is_valid():
            form_data = form.save(commit=False)
            form_data.user = request.user
            form_data.save()
            # text = form.cleaned_data['post']
            # form = boundForm()
        return redirect('/forms_test/form/cb/basic/')#('/forms_test/form/basic/')
        #     # args = {'form': form, 'text': text}
        #     # author.name = name
        #     # author.save()
        # context = {'objs':Author.objects.all()}
        # return render(request, 'forms_test/bound_form.html', context=context)
        #
