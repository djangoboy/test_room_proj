from django import forms
from .models import Author, Post


class AuthorForm(forms.ModelForm):
    class Meta:
        model = Author
        fields = ['name', 'phone', 'email', 'address', 'desc']


class UnboundForm(forms.Form):
    name = forms.CharField()

class PostunboundForm(forms.Form):
    post = forms.CharField()


class PostboundForm(forms.ModelForm):
    post = forms.CharField()
    class Meta:
        model = Post
        fields = ('post',)


class boundForm(forms.ModelForm):
    class Meta:
        model = Author
        fields = '__all__'


