from django.db import models

#for Author Model Class

class ActiveAuthorManager(models.Manager):
    def active(self):
        return self.get_queryset().filter(active=True)

    def mesingh(self):
        return self.get_queryset().filter(name='me singh')

#so essentially == Author.objects.all().filter(active=True)