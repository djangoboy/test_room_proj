from django.conf.urls import url, include
from rest_framework import routers #what is this for
from . import views


urlpatterns = (
    # urls for Author
    url(r'^author/cb/list/$', views.AuthorListView.as_view(), name='forms_test_author_list'),
    url(r'^author/fb/list/$', views.authorListView, name='authorListView'),
    url(r'^post/cb/list/$', views.PostListView.as_view(), name='PostListView'),
    url(r'^post/cb/protected_list/$', views.Protected_post_list, name='protected_post_list'),

    url(r'^author/cb/create/$', views.AuthorCreateView.as_view(), name='forms_test_author_create'),
    # url(r'^author/fb/create/$', views.authorCreateView, name='authorCreateView'),

    url(r'^author/cb/detail/(?P<pk>\S+)/$', views.AuthorDetailView.as_view(), name='forms_test_author_detail'),
    url(r'^author/fb/detail/(?P<pk>[0-9]+)/$', views.authorDetailView, name='authorDetailView'),

    #url(r'^author/p_detailView/(?P<pk>\S+)/$', views.ProtectedDetailView, name='ProtectedDetailView'),
    url(r'^author/update/(?P<pk>\S+)/$', views.AuthorUpdateView.as_view(), name='forms_test_author_update'),
    url(r'^form/unbound/', views.unbound_form, name='unbound_form'),
    url(r'^form/bound/', views.bound_form, name='bound_form'),
    #url(r'^form/basic/', views.home_basic_form, name='home_basic_form'),
    url(r'^form/cb/basic/', views.HomeView.as_view(), name='home_basic_form'),


)

