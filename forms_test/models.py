from django.core.urlresolvers import reverse
from django_extensions.db.fields import AutoSlugField
from django.db.models import *
from django.db import models as models
from django_extensions.db import fields as extension_fields
from .my_managers import ActiveAuthorManager

#we use model Manager to override some or any of the method or value of attribute that exists in Manager class

class Author(models.Model):
    name = models.CharField(max_length=255)
    phone = models.CharField(max_length=20, blank=True)
    email = models.EmailField(max_length=100, blank=True)
    address = models.CharField(max_length=300)
    desc = models.CharField(max_length=500)
    active = models.BooleanField(default=False)
    objects = ActiveAuthorManager()

    class Meta:
        ordering = ('-pk',)

    def __str__(self):
        return u'%s' % self.name

    def get_absolute_url(self):
        return reverse('forms_test:forms_test_author_detail', args=(self.pk,))

    def get_update_url(self):
        return reverse('forms_test:forms_test_author_update', args=(self.pk,))


class Form_Data(models.Model):

    # Fields
    name = models.CharField(max_length=255)

    class Meta:
        ordering = ('-pk',)

    def __str__(self):
        return u'%s' % self.name

    def get_absolute_url(self):
        return reverse('forms_test:home_form')

    def get_update_url(self):
        return reverse('forms_test:home_form')


class Post(models.Model):
    post = models.CharField(max_length=255)
    created_date = models.DateField(auto_now=False, auto_now_add=True, editable=False)
    last_updated = models.DateField(auto_now=True, auto_now_add=False, editable=False)
    author = models.ForeignKey(Author, on_delete=models.CASCADE)


    class Meta:
        ordering = ('-pk',)

    # def __str__(self):
    #     return u'%s' % self.name
    #
    # def get_absolute_url(self):
    #     return reverse('forms_test:home_form')
    #
    # def get_update_url(self):
    #     return reverse('forms_test:home_form')
    #
