from django.apps import AppConfig


class ClassNFuncsConfig(AppConfig):
    name = 'miscellaneous'
