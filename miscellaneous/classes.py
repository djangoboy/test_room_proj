#this is an example of setter & getter
class EmployeeParent:
    raise_amt_mo = 1.04
    def __init__(self, f_name, l_name, pay, dept):
        self.f_name = f_name
        self.l_name = l_name
        self.pay = pay
        self.dept = dept
        # self.email = f_name + '_' + l_name + '@gmail.com'

    #--> removed @property--> making it method
    def email(self):
        return self.f_name + '_' + self.l_name + '@gmail.com'

    def apply_raise(self):
        return self.pay + self.pay * self.raise_amt_mo

    @classmethod #<-- ACTING AS ALTERNATE INITIALIZER
    def set_raise_amt(cls, ammount):
        cls.raise_amt_mo = ammount

    @property
    def full_name(self):
        return self.f_name + '_' + self.l_name

    @full_name.setter
    def full_name(self, name):
        f_name, l_name = name.split(' ')
        self.f_name = f_name
        self.l_name = l_name

e1 = EmployeeParent('sushant','singh',2500, 'software')
e2 = EmployeeParent('deepak','singh',1000, 'software')


print(e1.apply_raise())
EmployeeParent.set_raise_amt(1.08)
# e1.raise_amt_mo = 1.09
print(e1.apply_raise())



























# print(e1.raise_amt_mo)
# print(e2.raise_amt_mo)
# EmployeeParent.raise_amt_mo = 6000
# # EmployeeParent.raise_amt_mo = 6000
# print(e1.raise_amt_mo)
# print(e2.raise_amt_mo)


        #output
# emp_1 --> <__main__.EmployeeParent object at 0x7f88c56466a0>
# emp_1.f_nameMukesh
# emp_1 --> <__main__.EmployeeParent object at 0x7f88c56466a0>
# emp_1.f_name -->Mukesh Singh
# emp_1.pay515215
# emp_1.pay 515215
# emp_1.emailDeepak_Singh@gmail.com

# class EmployeeParent:
#     def __init__(self, f_name, l_name, pay, dept):
#         self.f_name = f_name
#         self.l_name = l_name
#         self.pay = pay
#         self.dept = dept
#         self.email = f_name + '_'+ l_name + '@gmail.com'
#
# # class EmployeeParent(object):
# #     pass
#
# # emp_1 = EmployeeParent(f_name='Deepak', l_name='Singh', pay=515215, dept='software')
# # print(emp_1)
# # print('FULL NAME : '+ emp_1.f_name, emp_1.l_name)
# # print('PAY : '+ str(emp_1.pay))
# # print('DEPT : '+ emp_1.dept)
# # print('EMAIL : '+ emp_1.email)
#
# #what if....
#
# emp_1 = EmployeeParent(f_name='Deepak', l_name='Singh', pay=515215, dept='software')
# print('emp_1 -->', emp_1)
# emp_1.f_name = 'Mukesh'
# print('emp_1.f_name'+ emp_1.f_name)
# print('emp_1 -->', emp_1)
# print('emp_1.f_name -->'+ emp_1.f_name, emp_1.l_name)
# print('emp_1.pay'+ str(emp_1.pay))
# print('emp_1.pay',  emp_1.pay)
# print('emp_1.email'+ emp_1.email)
#
# #output
# # emp_1 --> <__main__.EmployeeParent object at 0x7f88c56466a0>
# # emp_1.f_nameMukesh
# # emp_1 --> <__main__.EmployeeParent object at 0x7f88c56466a0>
# # emp_1.f_name -->Mukesh Singh
# # emp_1.pay515215
# # emp_1.pay 515215
# # emp_1.emailDeepak_Singh@gmail.com
#









# Need to cover
    #MultiInheritence
    #decorators
    #generators
    #first_class functions
    # mixins