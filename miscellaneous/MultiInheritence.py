from miscellaneous.classes import EmployeeParent

#let say we want to pass one more argument to developersub class (prog_lang)
class DeveloperSub(EmployeeParent):
    def __init__(self, f_name, l_name, dept, prog_lang, pay):
        super().__init__(f_name=f_name, l_name=l_name, dept=dept, pay=pay)
                                    # self.pay = int(pay/2)
        self.prog_lang = prog_lang

    def email(self):  #<-- THIS IS HOW WE OVERRIDE iNSTANCE METHOD  FROM PARENT CLASS
        return self.f_name + '-' + self.l_name + '@gmail.com'

d1 = DeveloperSub('sushant','singh', 'software', 'python', 2500)
e1 = EmployeeParent('raja','singh', 'admin', 5)

